package com.example.selenium.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class SeleniumApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SeleniumApplication.class.getName());

	public static void main(String[] args) {
		SpringApplication.run(SeleniumApplication.class, args);
	}

	@Value("${username}")
	private String username;

	@Value("${password}")
	private String password;

	@Value("${apiKey}")
	private String apiKey;

	@PostConstruct
	public void run() throws Exception {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--remote-allow-origins=*");
		WebDriver driver = new ChromeDriver(options);
		driver.get("https://thedataextractors.com/fast-captcha/");
		WebElement usernameElement = driver.findElement(By.id("username"));
		WebElement passwordElement = driver.findElement(By.id("password"));
		usernameElement.sendKeys(username);
		passwordElement.sendKeys(password);

		com.fast.captcha.model.FastCaptchaResponse response = com.fast.captcha.CaptchaSolver.solve(apiKey,
				"6Lfur74oAAAAAHCXU97MyH0kgg0sx1uFnfJjs-B5", "https://thedataextractors.com/");
		if(!"SUCCESS".equals(response.getStatus())) {
			LOGGER.error("Response is not successful {}", response);
			throw new RuntimeException("Captcha solution not found.");
		}
		String solution = response.getSolution();
		//enter it in text area
		WebElement captchaSolutionElement = driver.findElement(By.id("g-recaptcha-response"));
		String javaScript = "arguments[0].style.height = 'auto'; arguments[0].style.display = 'block';";
		((JavascriptExecutor) driver).executeScript(javaScript, captchaSolutionElement);
		captchaSolutionElement.sendKeys(solution);

		//way-1
		passwordElement.submit();
		//way-2
//		WebElement loginButtonElement = driver.findElement(By.xpath("//button[text()=\"Login\"]"));
//		loginButtonElement.click();
		//way-3
//		WebElement formElement = driver.findElement(By.id("registrationForm"));
//		formElement.submit();
		Thread.sleep(5000);
		driver.quit();
	}

}
